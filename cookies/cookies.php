<?php
    if (isset($_POST['nombre'])){
        // Recibir del formulario el campo nombre
        $nombre = $_POST['nombre'];
        if (!isset($_COOKIE['nombre'])){
            // Crear la cookie (nombre de la cookie, valor a guardar, tiempo de expiración)
            setcookie('nombre', $nombre, time() + 4800);
        }
    }
    // Validar si la cookie existe y imprimir el valor
    if (isset($_COOKIE['nombre'])){
        echo 'Bienvenido '.$_COOKIE['nombre'];
    }else{
        echo 'Bienvenido anonimo';
    }
    echo '<br>';
    echo '<a href="eliminarCookie.php"> Salir </a>';
?>