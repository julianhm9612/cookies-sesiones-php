<?php
    // Si la cookie supera el tiempo de expiración, se elimina automaticamente
    // Para eliminar la cookie de forma manual, al tiempo de expiración se le agrega un tiempo en negativo
    setcookie('nombre', '', time() - 4800);
    echo 'Has salido del sistema';
    echo '<br>';
    echo '<a href="cookies.html"> Volver </a>';
?>