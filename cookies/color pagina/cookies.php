<?php 
    if(isset($_POST['color'])){
       $color = $_POST['color'];
       setcookie("color", $color);
    }else{
        if(isset($_COOKIE['color'])){
            $color = $_COOKIE['color'];
        }else{
            $color = 'white';
        }
    }
?>
<html>
    <head>
        <title>Cookies</title>
        <meta charset="UTF-8" />
    </head>
    <body <?php echo "style='background-color: $color'" ?>>
        <form action="cookies.php" method="post">
            <label for="color">Escoge un color de fondo: </label>
            <select name="color" id="color">
                <option value="red">Rojo</option>
                <option value="black">Negro</option>
                <option value="white">Blanco</option>
                <option value="gray">Gris</option>
            </select>
            <input type="submit" value="Cambiar color de fondo">
        </form>
    </body>
</html>